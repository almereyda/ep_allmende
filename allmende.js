var eejs = require('ep_etherpad-lite/node/eejs/');
// Add allmende css on home page
exports.eejsBlock_indexWrapper = function (hook_name, args, cb) {
    args.content = args.content + eejs.require("ep_allmende/templates/index.ejs");
    args.content = args.content + '<link href="../static/plugins/ep_allmende/static/css/index.css" rel="stylesheet">';
};
